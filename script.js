const WIDTH = 800;
const HEIGHT = 500;
const canvas = document.getElementById('canvas');
canvas.width = WIDTH;
canvas.height = HEIGHT;

const ctx = canvas.getContext('2d');
const array = [];
const N = 100;

class Point {
        constructor(point) {
        this.x = point.x;
        this.y = point.y;
    }
    static randomInteger(min, max) {
        return Math.floor(min + Math.random() * (max + 1 - min));
    }
    static random(xFrom, xTo, yFrom, yTo) {
        const x = this.randomInteger(xFrom, xTo);
        const y = this.randomInteger(yFrom, yTo);
        return new Point({x, y});
    }
}

class Line {
    constructor(point1, point2) {
        this.x1 = point1.x;
        this.y1 = point1.y;
        this.x2 = point2.x;
        this.y2 = point2.y;
    }
    get length() {
        return Math.sqrt((this.x2 - this.x1) ** 2 + (this.y2 - this.y1) ** 2);
    }
}

class Rectangle {
    constructor(point1, point2) {
        this.x1 = Math.min(point1.x, point2.x);
        this.y1 = Math.min(point1.y, point2.y);
        this.x2 = Math.max(point1.x, point2.x);
        this.y2 = Math.max(point1.y, point2.y);
        this.width = this.x2 - this.x1;
        this.height = this.y2 - this.y1;
    }
    drawRect(color) {
        ctx.strokeStyle = color;
        ctx.strokeRect(this.x1, this.y1, this.width, this.height);
    }
    isSquare() {
        return this.width === this.height;
    }
    get area() {
        return this.width * this.height;
    }
    contains(point) {
        return point.x > this.x1 && point.x < this.x2 && point.y > this.y1 && point.y < this.y2;
    }
    intersect(rect) {
         const x1 = Math.max(this.x1, rect.x1);
         const y1 = Math.max(this.y1, rect.y1);
         const x2 = Math.min(this.x2, rect.x2);
         const y2 = Math.min(this.y2, rect.y2);
         const width = x2 - x1;
         const height = y2 - y1;
         if(width >= 0 && height >= 0) return new Rectangle({x: x1, y: y1}, {x: x2, y: y2});
    }

}

class Ellipse {
    constructor(cx, cy, rx, ry) {
        this.cx = cx;
        this.cy = cy;
        this.rx = rx;
        this.ry = ry;
    }
    drawEllipse(color) {
        ctx.beginPath();
        ctx.strokeStyle = color;
        ctx.ellipse(this.cx, this.cy, this.rx, this.ry, Math.PI, 0, 2 * Math.PI);
        ctx.stroke();
    }
    generatePoints(arr, n) {
        for(let i = 0; i < n; i++) {
            const t = 2 * Math.PI * Math.random();
            const d = Math.sqrt(Math.random());
            const x = this.cx + this.rx * d * Math.cos(t);
            const y = this.cy + this.ry * d * Math.sin(t);
            arr.push({x: x, y: y});
        }
    }
    drawPoint(arr, color) {
        for(let i = 0; i < arr.length; i++) {
            ctx.beginPath();
            ctx.strokeStyle = color;
            ctx.arc(arr[i].x, arr[i].y, 1, 0, 2 * Math.PI);
            ctx.stroke();
        }
    }
    isCircle() {
        return this.rx === this.ry;
    }
    get area() {
        return this.rx * this.ry * Math.PI;
    }
    contains(point) {
        return (this.cx - point.x) ** 2 / this.rx ** 2 + (this.cy - point.y) ** 2 / this.ry ** 2 < 1;
    }
}

const rectangle1 = new Rectangle(Point.random(0, WIDTH, 0, HEIGHT), Point.random(0, WIDTH, 0, HEIGHT));
const rectangle2 = new Rectangle(Point.random(0, WIDTH, 0, HEIGHT), Point.random(0, WIDTH, 0, HEIGHT));

rectangle1.drawRect();
rectangle2.drawRect();

const rectangle3 = rectangle1.intersect(rectangle2);

if (rectangle3 !== undefined) {

    rectangle3.drawRect("red");

    const ellipse1 = new Ellipse(((rectangle3.x2 - rectangle3.x1) / 2) + rectangle3.x1,
        ((rectangle3.y2 - rectangle3.y1) / 2) + rectangle3.y1,
        (rectangle3.x2 - rectangle3.x1) / 2,
        (rectangle3.y2 - rectangle3.y1) / 2);

    ellipse1.drawEllipse("green");
    ellipse1.generatePoints(array, N);
    ellipse1.drawPoint(array, "blue");
}

